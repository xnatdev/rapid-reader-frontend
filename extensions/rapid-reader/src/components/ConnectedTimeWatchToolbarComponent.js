import { connect } from 'react-redux';
import { withSnackbar } from '@ohif/ui/src/contextProviders/SnackbarProvider';
import TimeWatchToolbarComponent from './TimeWatchToolbarComponent';
import { actions, fetchWorkList } from '../index';

const mapStateToProps = state => {
  const {
    rapidReader: { workList },
    authentication: {
      user: { xnatUrl },
    },
  } = state;

  return {
    workList: workList,
    xnatUrl: xnatUrl,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setWorkList: items => {
      dispatch(actions.setWorkList(items));
    },
    fetchWorkList,
  };
};

const ConnectedTimeWatchToolbarComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeWatchToolbarComponent);

export default withSnackbar(ConnectedTimeWatchToolbarComponent);
