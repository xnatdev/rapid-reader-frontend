import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './ReasonComponent.styl';

export function ReasonComponent({ handleSubmit, closeModal, desc }) {
  const [loading, setLoading] = useState(false);
  const [reason, setReason] = useState('');

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      await handleSubmit({ reason });
      setLoading(false);
      closeModal();
    } catch (error) {
      setLoading(false);
    }
  }

  function validateForm() {
    return reason && reason.length > 0;
  }

  return (
    <div className="rapid-comment-form">
      {desc && <p>{desc}</p>}
      <form onSubmit={onSubmit}>
        <FormField title="Reason">
          <textarea
            onChange={e => setReason(e.target.value)}
            className="rapid-comment-input"
          />
        </FormField>
        <div className="footer">
          <button
            className="submit-btn"
            type="submit"
            disabled={loading || !validateForm()}
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
}

ReasonComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  desc: PropTypes.string,
};

function FormField({ title, children, error }) {
  return (
    <label>
      <span>{title}</span>
      {children}
      {error && <p className="rapid-error">{error}</p>}
    </label>
  );
}

FormField.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  error: PropTypes.string,
};

export default ReasonComponent;
