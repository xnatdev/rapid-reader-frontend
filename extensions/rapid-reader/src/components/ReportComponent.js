import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Document, Page } from 'react-pdf';
import { Icon } from '@ohif/ui';

import './ReportComponent.styl';
import { useEffect } from 'react';

import { createPdf } from '../index';

export function ReportComponent({
  handleSubmit,
  closeModal,
  readerFullName,
  workListId,
  finishedDate,
  workItems,
}) {
  const rootDiv = useRef(null);
  const [pdf, setPdf] = useState(undefined);
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [width, setWidth] = useState(1);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const output = createPdf(
      readerFullName,
      workListId,
      finishedDate,
      workItems
    );
    setPdf(output);
  }, [finishedDate, readerFullName, workItems, workListId]);

  useEffect(() => {
    const width = rootDiv.current.offsetWidth;
    setWidth(width);
  }, []);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);

    try {
      await handleSubmit(pdf);
    } finally {
      setLoading(false);
      closeModal();
    }
  }

  return (
    <div ref={rootDiv} className="report-form">
      <form onSubmit={onSubmit}>
        <div className="header">
          <button className="sign-btn" type="submit" disabled={loading}>
            {!loading && 'Sign'}
            {loading && (
              <div>
                <Icon name="circle-notch" />
              </div>
            )}
          </button>
        </div>
        <Document file={pdf} onLoadSuccess={onDocumentLoadSuccess}>
          <Page pageNumber={pageNumber} width={width} />
        </Document>
        <p style={{ textAlign: 'center' }}>
          {pageNumber} of {numPages}
        </p>
      </form>
    </div>
  );
}

ReportComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  xnatUrl: PropTypes.string.isRequired,
  readerFullName: PropTypes.string.isRequired,
  workListId: PropTypes.string.isRequired,
  finishedDate: PropTypes.object.isRequired,
  workItems: PropTypes.object.isRequired,
  uploadPdf: PropTypes.func.isRequired,
};

export default ReportComponent;
