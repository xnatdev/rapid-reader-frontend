import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './ConfirmComponent.styl';

export function ConfirmComponent({ handleSubmit, closeModal, fields }) {
  const [loading, setLoading] = useState(false);

  async function onSubmit(e) {
    e.preventDefault();
    setLoading(true);
    try {
      await handleSubmit();
      setLoading(false);
      closeModal();
    } catch (error) {
      setLoading(false);
    }
  }

  return (
    <div className="rapid-confirm-form">
      <div className="body">
        <div>Please confirm your assessment.</div>
        <Fields fields={fields} />
      </div>

      <form onSubmit={onSubmit}>
        <div className="footer">
          <button className="submit-btn" type="submit">
            Confirm
          </button>
        </div>
      </form>
    </div>
  );
}

ConfirmComponent.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  fields: PropTypes.array,
};

function Fields({ fields }) {
  if (!fields) {
    return null;
  }
  return (
    <>
      {fields.map(item => (
        <Field key={item.id} item={item} />
      ))}
    </>
  );
}

Fields.propTypes = {
  fields: PropTypes.array,
};

function Field({ item }) {
  function camelize(text) {
    if (!text || typeof text !== 'string' || !text.length) {
      return text;
    }
    return text[0].toUpperCase() + text.substring(1).toLowerCase();
  }
  return (
    <div className="field">
      <span className="cell">{camelize(item.name)}</span>
      <span className="cell">{item.value}</span>
    </div>
  );
}

Field.propTypes = {
  item: PropTypes.object,
};

export default ConfirmComponent;
