import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import RapidWorkItem from './RapidWorkItem';
import { WorkItemStatus } from '../constants';

import './RapidWorkItemsPanel.css';

function RapidWorkItemsPanel({
  workList,
  currentWorkItemIdx,
  currentWorkItemId,
  workItemStatus,
  filteredItems,
  setWorkItemStatus,
  moveWorkItem,
}) {
  const totalItems = workList.items || [];
  const [existingStatusMap, setExistingStatusMap] = useState({});

  useEffect(() => {
    const statusMap = {};
    workList.items.forEach(item => {
      if (!statusMap[item.status]) {
        statusMap[item.status] = 1;
      } else {
        statusMap[item.status]++;
      }
    });
    setExistingStatusMap(statusMap);
  }, [workList]);

  function onStatusChange(e) {
    const status = e.target.value;
    setWorkItemStatus(status);
  }
  return (
    <div className="rapid-navigation-tree">
      <ul>
        <h4>Work Item List</h4>
        <div className="rapid-filter-row">
          <div className="rapid-filter-name">Filter</div>
          <div className="rapid-filter-value">
            <select onChange={onStatusChange} value={workItemStatus}>
              <option value="">All ({totalItems.length})</option>
              {Object.keys(WorkItemStatus).map(status => (
                <option
                  key={status}
                  value={status}
                  disabled={
                    !Object.keys(existingStatusMap).find(s => s === status)
                  }
                >
                  {status} (
                  {existingStatusMap[status] ? existingStatusMap[status] : ''})
                </option>
              ))}
            </select>
          </div>
        </div>

        {filteredItems &&
          filteredItems.map((item, idx) => (
            <li key={`${item.id}-${item.status}`}>
              <RapidWorkItem
                item={item}
                pos={idx}
                isActive={currentWorkItemId === item.id}
                onClick={moveWorkItem.bind(undefined, idx)}
              />
            </li>
          ))}
        {!!filteredItems.length || <div>Not Found</div>}
      </ul>
    </div>
  );
}

RapidWorkItemsPanel.propTypes = {
  workList: PropTypes.object.isRequired,
  currentWorkItemIdx: PropTypes.number.isRequired,
  currentWorkItemId: PropTypes.number.isRequired,
  workItemStatus: PropTypes.string.isRequired,
  filteredItems: PropTypes.array.isRequired,
  setWorkItemStatus: PropTypes.func.isRequired,
  moveWorkItem: PropTypes.func.isRequired,
};

export default RapidWorkItemsPanel;
