import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Icon, useModal } from '@ohif/ui';
import { ScrollableArea } from '@ohif/ui/src/ScrollableArea/ScrollableArea';
import { TableList } from '@ohif/ui/src/components/tableList';
import dompurify from 'dompurify';
import { WorkItemStatus } from '../constants';
import ReasonComponent from './ReasonComponent';
import ConfirmComponent from './ConfirmComponent';
import './RapidRadReportPanel.styl';

function RapidRadReportPanel({
  workItemStatus,
  radReportData,
  formDisabled,
  requiredFieldIds,
  evaluating,
  times,
  fetchPostAssessment,
  fetchGetAssessment,
  fetchUpdateWorkItemStatus,
  isLastItem,
  fetchBeginTimeTrack,
  fetchUpdateTimeTrack,
  hasSubmittedAssessment,
  filteredStatus,
  setWorkItem,
  displayMessage,
  moveToNextWorkItem,
  modal,
}) {
  const reportDiv = useRef();
  const [radReportTemplateData, setRadReportTemplateData] = useState();
  const [assessment, setAssessment] = useState();
  const [loading, setLoading] = useState(false);
  const { show: showModal, hide: hideModal } = useModal();

  const failable =
    workItemStatus === WorkItemStatus.Open ||
    workItemStatus === WorkItemStatus.Partial;
  const resumable = !evaluating && failable;

  /** Fill form area with the radreport html template */
  /* TODO: memoize radReportData */
  useEffect(() => {
    if (!radReportData || !radReportData.templateData) {
      return;
    }
    const radReportTemplateData = radReportData.templateData
      .split('<body>')[1]
      .split('</body>')[0];
    setRadReportTemplateData(radReportTemplateData);
  }, [radReportData]);

  /** Enable/disable form fields */
  /* TODO: memoize formDisabled, assessment */
  useEffect(() => {
    function enableDisableForm() {
      const domTags = getAllFormDomTags();
      [].map.call(domTags, tag => {
        tag.disabled = formDisabled;

        if (assessment) {
          const key = convertName(tag.name);
          if (assessment[key]) {
            tag.value = assessment[key];
          }
        }
      });
    }

    enableDisableForm(formDisabled);
    // Keep radReportTemplateData to enable/disable after radReportTemplateData being updated
  }, [formDisabled, assessment, radReportTemplateData]);

  /** Fill the radreport form with the assessment if exists */
  /* TODO: fetchGetAssessment, hasSubmittedAssessment */
  useEffect(() => {
    async function loadRadReportResult() {
      if (!hasSubmittedAssessment) {
        return;
      }
      try {
        setAssessment(await fetchGetAssessment());
      } catch (error) {
        setAssessment(undefined);
      }
    }
    loadRadReportResult();
  }, [fetchGetAssessment, hasSubmittedAssessment]);

  function getRadReportTemplateForm() {
    if (!radReportData) {
      return (
        <div className="radReportLoading">
          <div className="image-thumbnail-loading-indicator"></div>
        </div>
      );
    }

    return (
      <>
        {resumable && (
          <div className="error-status">
            <div className="error-border">
              The evaluation was interrupted. Please click the button below to
              resume.
            </div>
          </div>
        )}
        {formDisabled && hasSubmittedAssessment && !assessment && (
          <div className="error-status">
            <div className="error-border">
              The assessment is not found. This might be deleted in XNAT.
            </div>
          </div>
        )}
        {workItemStatus === WorkItemStatus.Failed && (
          <div className="error-status">
            <div className="error-border">
              You set this to failed. If you want to reassess this, click reopen
              button.
              <button
                onClick={handleReopen}
                className="reopenBtn"
                disabled={failable}
              >
                {loading && <Icon name="circle-notch" className="icon-spin" />}
                {!loading && (
                  <Icon name="exclamation-circle" width="14px" height="14px" />
                )}
                Repoen
              </button>
            </div>
          </div>
        )}
        <TableList
          customHeader={
            <div className="tableListHeaderTitle">{radReportData.title}</div>
          }
        >
          <div
            ref={reportDiv}
            className="radReportTableBody"
            dangerouslySetInnerHTML={{
              __html: dompurify.sanitize(radReportTemplateData),
            }}
          />
        </TableList>
      </>
    );
  }

  function convertName(raw) {
    return raw
      .trim()
      .replace(/\s+/g, '_')
      .toLowerCase();
  }

  function validate(formData) {
    let filteredFormData = formData;
    // If there are required field ids, only check them.
    if (requiredFieldIds) {
      const tokens = requiredFieldIds.split(',').map(token => token.trim());
      filteredFormData = formData.filter(item => tokens.includes(item.id));
    }

    const invalidated = filteredFormData.find(item => !item.value);
    if (invalidated) {
      throw new Error(`${invalidated.originalName} is empty.`);
    }
  }

  function onSaveClicked() {
    showModal({
      content: ConfirmComponent,
      contentProps: {
        handleSubmit: handleSave,
        closeModal: hideModal,
        fields: getFormData(),
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: 'Save Confirmation',
    });
  }

  function getFormData() {
    const domTags = getAllFormDomTags();
    const formData = [].map.call(domTags, tag => ({
      id: tag.id,
      originalName: tag.name,
      name: convertName(tag.name),
      node: tag.nodeName,
      value: tag.value,
    }));
    formData.forEach(item => {
      if (!item.name) {
        item.name = item.id;
      }
    });
    return formData;
  }

  async function handleSave() {
    const formData = getFormData();

    setLoading(true);
    try {
      validate(formData);
      const workItem = await fetchPostAssessment(formData);
      setWorkItem(workItem);

      if (isLastItem()) {
        displayMessage({
          title: 'Congratulation, finished!!!',
          type: 'success',
        });
      } else {
        // When filter is set, don't move because the updated one will be filtered out.
        if (!filteredStatus) {
          moveToNextWorkItem();
        }
      }
    } catch (error) {
      displayMessage({
        title: 'Save RadReport',
        message: error.message,
        type: 'error',
      });
    }
    setLoading(false);
  }

  async function openFailModal() {
    modal.show({
      content: ReasonComponent,
      contentProps: {
        handleSubmit: handleFail,
        closeModal: modal.hide,
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: 'Reason for Fail',
    });
  }

  async function handleFail({ reason }) {
    setLoading(true);
    try {
      const workItem = await fetchUpdateWorkItemStatus(
        WorkItemStatus.Failed,
        reason
      );
      setWorkItem(workItem);

      if (isLastItem()) {
        displayMessage({
          title: 'Congratulation, finished!!!',
          type: 'success',
        });
      } else {
        // When filter is set, don't move because the updated one will be filtered out.
        if (!filteredStatus) {
          moveToNextWorkItem();
        }
      }
    } catch (error) {
      displayMessage({
        title: 'Save RadReport',
        message: error.message,
        type: 'error',
      });
    }
    setLoading(false);
  }

  async function handleReopen() {
    setLoading(true);
    try {
      const workItem = await fetchUpdateWorkItemStatus(WorkItemStatus.Open);
      setWorkItem(workItem);
    } catch (error) {
      displayMessage({
        title: 'Save RadReport',
        message: error.message,
        type: 'error',
      });
    }
    setLoading(false);
  }

  async function handleResume() {
    await fetchBeginTimeTrack();
    await refreshWorkList();
  }

  async function refreshWorkList() {
    location.reload();
  }

  async function onInterruptClicked() {
    showModal({
      content: ReasonComponent,
      contentProps: {
        handleSubmit: handleStop,
        closeModal: hideModal,
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: 'Reason for Interruption',
    });
  }

  async function handleStop({ reason }) {
    if (times.length === 0) {
      return;
    }
    const lastTime = times[times.length - 1];

    await fetchUpdateTimeTrack({
      id: lastTime.id,
      beginDate: lastTime.beginDate,
      endDate: Date.now(),
      abnormallyStopped: true,
      stopReason: reason,
    });
    await refreshWorkList();
    location.href = '/app/action/LogoutUser';
  }

  async function onResumeClicked() {
    if (times.length === 0) {
      await handleResume();
      return;
    }

    const lastTime = times[times.length - 1];
    if (lastTime.stopReason) {
      await handleResume();
      return;
    }

    showModal({
      content: ReasonComponent,
      contentProps: {
        handleSubmit: async ({ reason }) => {
          try {
            await handleUpdateReasonOnly({
              reason,
            });
            await handleResume();
          } catch (error) {
            console.log(error);
          }
        },
        closeModal: hideModal,
      },
      shouldCloseOnEsc: true,
      isOpen: true,
      closeButton: true,
      title: 'Reason for Stop',
    });
  }

  async function handleUpdateReasonOnly({ reason }) {
    if (times.length === 0) {
      return;
    }
    const lastTime = times[times.length - 1];

    await fetchUpdateTimeTrack({
      id: lastTime.id,
      beginDate: lastTime.beginDate,
      endDate: lastTime.endDate,
      abnormallyStopped: true,
      stopReason: reason,
    });
    await refreshWorkList();
  }

  function getAllFormDomTags() {
    const selected = document.querySelectorAll(
      '.radReportTable input[type=text], .radReportTable input[type=number], .radReportTable input[type=radio], .radReportTable select, .radReportTable textarea'
    );
    return [].filter.call(selected, tag => {
      if (tag.nodeName.toLowerCase() !== 'input' || tag.type !== 'radio') {
        return true;
      }
      return tag.checked;
    });
  }

  return (
    <div className="radReportTable">
      <ScrollableArea>
        <div>{getRadReportTemplateForm()}</div>
      </ScrollableArea>
      <div className="radReportTableFooter">
        <button
          onClick={onSaveClicked}
          className="saveBtn"
          disabled={!evaluating || formDisabled || loading}
        >
          {loading && <Icon name="circle-notch" className="icon-spin" />}
          {!loading && <Icon name="save" width="14px" height="14px" />}
          Save {!isLastItem() && '& Next'}
        </button>

        <button
          onClick={openFailModal}
          className="nextBtn"
          disabled={!evaluating || !failable}
        >
          {loading && <Icon name="circle-notch" className="icon-spin" />}
          {!loading && (
            <Icon name="exclamation-circle" width="14px" height="14px" />
          )}
          Fail {!isLastItem() && '& Next'}
        </button>

        {evaluating && (
          <button onClick={onInterruptClicked} className="interruptBtn">
            <Icon name="stop" width="14px" height="14px" />
            Interrupt session
          </button>
        )}

        {resumable && (
          <button onClick={onResumeClicked} className="resumeBtn">
            <Icon name="play" width="14px" height="14px" />
            Resume session
          </button>
        )}

        {/* {!isLastItem() && (
          <button onClick={moveToNextWorkItem} className="nextBtn">
            <Icon name="rapid-next" width="14px" height="14px" />
            Next
          </button>
        )} */}
      </div>
    </div>
  );
}

RapidRadReportPanel.propTypes = {
  workListStatus: PropTypes.string.isRequired,
  workItemStatus: PropTypes.string.isRequired,
  radReportData: PropTypes.object.isRequired,
  formDisabled: PropTypes.bool,
  hasSubmittedAssessment: PropTypes.bool.isRequired,
  filteredStatus: PropTypes.string,
  requiredFieldIds: PropTypes.string,
  evaluating: PropTypes.bool.isRequired,
  times: PropTypes.array.isRequired,
  fetchPostAssessment: PropTypes.func.isRequired,
  fetchUpdateWorkListStatus: PropTypes.func.isRequired,
  fetchGetAssessment: PropTypes.func.isRequired,
  fetchUpdateWorkItemStatus: PropTypes.func.isRequired,
  fetchWorkList: PropTypes.func.isRequired,
  isWorkListFinished: PropTypes.func.isRequired,
  isLastItem: PropTypes.func.isRequired,
  fetchBeginTimeTrack: PropTypes.func.isRequired,
  fetchUpdateTimeTrack: PropTypes.func.isRequired,
  setWorkItem: PropTypes.func.isRequired,
  setWorkList: PropTypes.func.isRequired,
  moveToNextWorkItem: PropTypes.func.isRequired,

  displayMessage: PropTypes.func.isRequired, // panelModule.js
  modal: PropTypes.object.isRequired,
};

export { RapidRadReportPanel };
export default RapidRadReportPanel;
