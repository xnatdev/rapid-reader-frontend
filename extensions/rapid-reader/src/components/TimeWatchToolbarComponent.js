import React, { useState } from 'react';
import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Icon, useModal } from '@ohif/ui';
import { calculatedElapsedInMs } from '../utils';
import { fetchBeginTimeTrack, fetchUpdateTimeTrack } from '../fetch';
import ReasonComponent from '../components/ReasonComponent';
import './TimeWatchToolbarComponent.css';

const TRACK_PERIOD_IN_SEC = 10;

export function TimeWatchToolbarComponent({
  workList,
  xnatUrl,
  fetchWorkList,
  setWorkList,
}) {
  const [timerOn, setTimerOn] = useState(false);
  const [elapsed, setElapsed] = useState(0);
  const [startedAt, setStartedAt] = useState(0);
  const [timerTime, setTimerTime] = useState(0);

  const { show: showModal, hide: hideModal } = useModal();

  useEffect(() => {
    const { times, evaluating } = workList;

    const calculatedElapsed = calculatedElapsedInMs(times);
    setElapsed(calculatedElapsed);
    if (times.length && evaluating) {
      setTimerOn(true);
      setStartedAt(Date.now());
    } else {
      setTimerTime(calculatedElapsed);
      setTimerOn(false);
    }
  }, [workList]);

  //   useEffect(() => {
  //     if (!timerOn) {
  //       return;
  //     }

  //     const timer = setInterval(async () => {
  //       setTimerTime(elapsed + Date.now() - startedAt);
  //     }, 500);

  //     return () => {
  //       clearInterval(timer);
  //     };
  //   }, [elapsed, startedAt, timerOn]);

  useEffect(() => {
    if (!timerOn) {
      return;
    }
    const timer = setInterval(async () => {
      const { times } = workList;
      if (times.length === 0) {
        return;
      }
      const lastTime = times[times.length - 1];
      await fetchUpdateTimeTrack(xnatUrl, workList.id, {
        id: lastTime.id,
        beginDate: lastTime.beginDate,
        endDate: Date.now(),
        abnormallyStopped: false,
      });
    }, TRACK_PERIOD_IN_SEC * 1000);

    return () => {
      clearInterval(timer);
    };
  }, [workList, timerOn, xnatUrl]);

  let seconds = ('0' + (Math.floor(timerTime / 1000) % 60)).slice(-2);
  let minutes = ('0' + (Math.floor(timerTime / 60000) % 60)).slice(-2);
  let hours = ('0' + Math.floor(timerTime / 3600000)).slice(-2);

  async function playOrStop() {
    if (timerOn) {
      showModal({
        content: ReasonComponent,
        contentProps: {
          handleSubmit: handleStop,
          closeModal: hideModal,
        },
        shouldCloseOnEsc: true,
        isOpen: true,
        closeButton: true,
        title: 'Reason for Interruption',
      });
    } else {
      // Resuming...
      const { times } = workList;
      if (times.length === 0) {
        await handleResume();
        return;
      }

      const lastTime = times[times.length - 1];
      if (lastTime.stopReason) {
        await handleResume();
        return;
      }

      showModal({
        content: ReasonComponent,
        contentProps: {
          handleSubmit: async ({ reason }) => {
            try {
              await handleUpdateReasonOnly({ reason });
              await handleResume();
            } catch (error) {
              console.log(error);
            }
          },
          closeModal: hideModal,
        },
        shouldCloseOnEsc: true,
        isOpen: true,
        closeButton: true,
        title: 'Reason for Stop',
      });
    }
  }

  async function handleStop({ reason }) {
    const { times } = workList;
    if (times.length === 0) {
      return;
    }
    const lastTime = times[times.length - 1];

    await fetchUpdateTimeTrack(xnatUrl, workList.id, {
      id: lastTime.id,
      beginDate: lastTime.beginDate,
      endDate: Date.now(),
      abnormallyStopped: true,
      stopReason: reason,
    });
    await refreshWorkList();
    location.href = '/app/action/LogoutUser';
  }

  async function handleUpdateReasonOnly({ reason }) {
    const { times } = workList;
    if (times.length === 0) {
      return;
    }
    const lastTime = times[times.length - 1];

    await fetchUpdateTimeTrack(xnatUrl, workList.id, {
      id: lastTime.id,
      beginDate: lastTime.beginDate,
      endDate: lastTime.endDate,
      abnormallyStopped: true,
      stopReason: reason,
    });
    await refreshWorkList();
  }

  async function handleResume() {
    await fetchBeginTimeTrack(xnatUrl, workList.id);
    await refreshWorkList();
  }

  async function refreshWorkList() {
    const refreshedWorkList = await fetchWorkList(xnatUrl, workList.id);
    setWorkList(refreshedWorkList);
  }

  return (
    <div className="Stopwatch">
      <button
        title={'Play / Stop'}
        className="btn"
        data-toggle="tooltip"
        onClick={playOrStop}
      >
        <Icon
          name={timerOn ? 'stop' : 'play'}
          className={timerOn ? 'stop' : 'play'}
        />
      </button>
      {/* <div className="Stopwatch-display">
        {hours} : {minutes} : {seconds}
      </div> */}
    </div>
  );
}

TimeWatchToolbarComponent.propTypes = {
  workList: PropTypes.any.isRequired,
  xnatUrl: PropTypes.string.isRequired,
  fetchWorkList: PropTypes.func.isRequired,
  setWorkList: PropTypes.func.isRequired,
};

export default TimeWatchToolbarComponent;
