import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { setWorkItemIdx } from '../redux/actions';
import './CurrentWorkItemToolbarComponent.css';

export function CurrentWorkItemToolbarComponent({
  filteredItems,
  currentWorkItemIdx,
}) {
  const elCurrentWorkItem = useRef(null);
  const [currentPosition, setCurrentPosition] = useState(0);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    setTotalCount(filteredItems.length);
    setCurrentPosition(currentWorkItemIdx + 1);
  }, [currentWorkItemIdx, filteredItems]);

  function handleChange(e) {
    try {
      const val = parseInt(e.target.value);
      if (isNaN(val) || val <= 0) {
        setCurrentPosition('');
      } else {
        setCurrentPosition(val);
      }
    } catch (err) {
      setCurrentPosition('');
    }
  }

  function handleKeyDown(e) {
    const key = e.keyCode || e.which;
    if (key === 13) {
      // when 'enter' pressed, apply change.
      moveWorkItem();
    } else if (key === 27) {
      // when 'esc' pressed, cancel change.
      cancelPositionChange();
    }
  }

  function handleBlur(e) {
    cancelPositionChange();
  }

  function cancelPositionChange() {
    setCurrentPosition(
      window.store.getState().rapidReader.currentWorkItemIdx + 1
    );
  }

  function moveWorkItem() {
    let originalPosition =
      window.store.getState().rapidReader.currentWorkItemIdx + 1;
    let adjustedPosition = currentPosition;
    if (!currentPosition || isNaN(currentPosition)) {
      adjustedPosition = originalPosition;
    } else if (currentPosition <= 0) {
      adjustedPosition = 1;
    } else if (currentPosition > totalCount) {
      adjustedPosition = totalCount;
    }
    setCurrentPosition(adjustedPosition);
    if (adjustedPosition === originalPosition) {
      return;
    }
    window.store.dispatch(setWorkItemIdx(adjustedPosition - 1));
  }

  return (
    <div className="CurrentWorkItemBox">
      <input
        ref={elCurrentWorkItem}
        type="text"
        className="CurrentWorkItem"
        value={currentPosition}
        // onChange={handleChange}
        // onKeyDown={handleKeyDown}
        // onBlur={handleBlur}
      />
      <div> / </div>
      <div className="TotalWorkItem">{totalCount}</div>
    </div>
  );
}

CurrentWorkItemToolbarComponent.propTypes = {
  filteredItems: PropTypes.array.isRequired,
  currentWorkItemIdx: PropTypes.number.isRequired,
};

export default CurrentWorkItemToolbarComponent;
