import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';
import EmailChip from './EmailChip';

import './IssuesComponent.styl';

export function IssuesComponent({ fromEmail, handleSend, closeModal }) {
  const [loading, setLoading] = useState(false);
  const [toEmails, setToEmails] = useState([]);
  const [subject, setSubject] = useState('');
  const [body, setBody] = useState('');

  async function onClickSend() {
    setLoading(true);
    try {
      await handleSend({ subject, toEmails, body });
      setLoading(false);
      closeModal();
    } catch (error) {
      setLoading(false);
    }
  }

  function handleDeleteFromToEmails(email) {
    setToEmails(toEmails.filter(item => item !== email));
  }

  function handleAddEmail(email) {
    if (Array.isArray(email)) {
      setToEmails([...toEmails, ...email]);
    } else {
      setToEmails([...toEmails, email]);
    }
  }

  function validateForm() {
    return (
      toEmails &&
      toEmails.length > 0 &&
      subject &&
      subject.trim().length > 0 &&
      body &&
      body.trim().length > 0
    );
  }

  return (
    <div className="rapid-comment-form">
      <FormField title="From">
        <input
          type="text"
          value={fromEmail}
          disabled={true}
          className="rapid-comment-input"
        />
      </FormField>
      <FormField title="Tos">
        <EmailChip
          items={toEmails}
          handleAdd={handleAddEmail}
          handleDelete={handleDeleteFromToEmails}
        />
      </FormField>
      <FormField title="Subject">
        <input
          type="text"
          onChange={e => setSubject(e.target.value)}
          className="rapid-comment-input"
        />
      </FormField>
      <FormField title="Body">
        <textarea
          onChange={e => setBody(e.target.value)}
          className="rapid-comment-input"
        />
      </FormField>
      <div>
        <button onClick={onClickSend} disabled={loading || !validateForm()}>
          Send
        </button>
      </div>
    </div>
  );
}

IssuesComponent.propTypes = {
  fromEmail: PropTypes.string.isRequired,
  handleSend: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};

function FormField({ title, children, error }) {
  return (
    <label>
      <span>{title}</span>
      {children}
      {error && <p className="rapid-error">{error}</p>}
    </label>
  );
}

FormField.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  error: PropTypes.string,
};

export default IssuesComponent;
