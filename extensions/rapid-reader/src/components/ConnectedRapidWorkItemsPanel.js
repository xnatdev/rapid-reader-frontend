import { connect } from 'react-redux';
import RapidWorkItemsPanel from './RapidWorkItemsPanel';
import { setWorkItemIdx, setWorkItemStatus } from '../redux/actions';

const mapStateToProps = state => {
  return {
    user: state.authentication.user,
    workList: state.rapidReader.workList,
    currentWorkItemIdx: state.rapidReader.currentWorkItemIdx,
    currentWorkItemId: state.rapidReader.currentWorkItemId,
    workItemStatus: state.rapidReader.workItemStatus,
    filteredItems: state.rapidReader.filteredItems,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    moveWorkItem: currentWorkItemIdx => {
      dispatch(setWorkItemIdx(currentWorkItemIdx));
    },
    setWorkItemStatus: status => {
      dispatch(setWorkItemStatus(status));
    },
  };
};

const ConnectedRapidWorkItemsPanel = connect(
  mapStateToProps,
  mapDispatchToProps
)(RapidWorkItemsPanel);

export default ConnectedRapidWorkItemsPanel;
