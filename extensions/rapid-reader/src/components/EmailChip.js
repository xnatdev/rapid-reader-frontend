import React from 'react';
import PropTypes from 'prop-types';
import { SpecialGroupEmails } from '../constants';
import './EmailChip.css';

export default class EmailChip extends React.Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    handleAdd: PropTypes.func.isRequired,
    handleDelete: PropTypes.func.isRequired,
  };

  state = {
    value: '',
    error: null,
  };

  handleKeyDown = evt => {
    if (['Enter', 'Tab', ','].includes(evt.key)) {
      const value = this.state.value.trim();
      const validated = value && this.isValid(value);
      if (
        ['Enter', ','].includes(evt.key) ||
        (evt.key === 'Tab' && (!validated && this.props.items.length === 0))
      ) {
        evt.preventDefault();
      }

      if (validated) {
        this.props.handleAdd(value);
        this.setState({
          value: '',
        });
      }
    }
  };

  handleChange = evt => {
    this.setState({
      value: evt.target.value,
      error: null,
    });
  };

  handlePaste = evt => {
    evt.preventDefault();

    var paste = evt.clipboardData.getData('text');
    var emails = paste.match(/[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/g);

    if (emails) {
      var toBeAdded = emails.filter(email => {
        return !this.isInList(email);
      });

      this.props.handleAdd(toBeAdded);
    }
  };

  isValid = email => {
    let error = null;

    if (this.isInList(email)) {
      error = `${email} has already been added.`;
    }

    const isEmailGroup =
      Object.keys(SpecialGroupEmails).findIndex(
        groupName => SpecialGroupEmails[groupName] === email
      ) >= 0;

    if (!isEmailGroup && !this.isEmail(email)) {
      error = `${email} is not a valid email address.`;
    }

    if (error) {
      this.setState({ error });

      return false;
    }

    return true;
  };

  isInList = email => {
    return this.props.items.includes(email);
  };

  isEmail = email => {
    return /[\w\d\.-]+@[\w\d\.-]+\.[\w\d\.-]+/.test(email);
  };

  handleAddGroup = groupValue => {
    if (this.isValid(groupValue)) {
      this.props.handleAdd(groupValue);
      this.setState({
        value: '',
      });
    }
  };

  render() {
    return (
      <>
        {this.props.items.map(item => (
          <div className="rapid-tag-item" key={item}>
            {item}
            <div
              className="button"
              onClick={() => this.props.handleDelete(item)}
            >
              &times;
            </div>
          </div>
        ))}
        <input
          className={
            'rapid-comment-input ' + (this.state.error && ' has-error')
          }
          value={this.state.value}
          placeholder="Type or paste email addresses and press `Enter`..."
          onKeyDown={this.handleKeyDown}
          onChange={this.handleChange}
          onPaste={this.handlePaste}
        />
        {this.state.error && <p className="rapid-error">{this.state.error}</p>}

        <div className="rapid-special-group">
          Special Email Groups:{' '}
          {Object.keys(SpecialGroupEmails).map(groupName => (
            <SpecialGroup
              key={groupName}
              groupName={groupName}
              groupValue={SpecialGroupEmails[groupName]}
              handleAddGroup={this.handleAddGroup}
            />
          ))}
        </div>
      </>
    );
  }
}

function SpecialGroup({ groupName, groupValue, handleAddGroup }) {
  return (
    <>
      <div
        className="rapid-group-button"
        onClick={() => handleAddGroup(groupValue)}
      >
        {groupName}
      </div>
    </>
  );
}

SpecialGroup.propTypes = {
  groupName: PropTypes.string.isRequired,
  groupValue: PropTypes.string.isRequired,
  handleAddGroup: PropTypes.func.isRequired,
};
