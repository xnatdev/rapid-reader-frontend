import { doGet, doPut, doPost, doDelete } from './util';

export async function fetchComments(xnatUrl, workItemId, filters) {
  const url = new URL(`${xnatUrl}/xapi/rapidReader/comments/${workItemId}/1`);
  if (filters) {
    Object.keys(filters).forEach(filter => {
      url.searchParams.append(filter, filters[filter]);
    });
  }

  const response = await doGet(url.href);
  return await response.json();
}

export async function fetchComment(xnatUrl, commentId) {
  const url = `${xnatUrl}/xapi/rapidReader/comments/${commentId}`;
  const response = await doGet(url);
  return await response.json();
}

export async function fetchCreateComment(xnatUrl, body) {
  const url = `${xnatUrl}/xapi/rapidReader/comments`;
  const response = await doPost(url, body);
  return await response.json();
}

export async function fetchUpdateComment(xnatUrl, commentId, body) {
  const url = `${xnatUrl}/xapi/rapidReader/comments/${commentId}`;
  const response = await doPut(url, body);
  return await response.json();
}

export async function fetchUpdateCommentStatus(xnatUrl, commentId, status) {
  const url = `${xnatUrl}/xapi/rapidReader/comments/${commentId}/status/${status}`;
  const response = await doPut(url);
  return await response.json();
}

export async function fetchDeleteComment(xnatUrl, commentId) {
  const url = `${xnatUrl}/xapi/rapidReader/comments/${commentId}`;
  const response = await doDelete(url);
  return await response.json();
}
