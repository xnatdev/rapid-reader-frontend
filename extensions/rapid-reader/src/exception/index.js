export class CorsException extends Error {
  constructor() {
    super('XNAT blocks the CORS request. Please contact XNAT admin.');
  }
}

export class UserExpiredException extends Error {
  constructor() {
    super('User Expired');
  }
}

export class HttpException extends Error {
  constructor(status, statusText, body = undefined) {
    super(`[${status}]${statusText}`);
    this.body = body;
  }
}

export class PageNotFoundException extends Error {
  constructor() {
    super('Page Not Found');
  }
}

export class BadCredentialException extends Error {
  constructor(
    message = 'Your login attempt failed because the username and password combination you provided was invalid or your user already has the maximum number of user sessions open.',
    body = undefined
  ) {
    super(message);
    this.body = body;
  }
}

export class DevProxyException extends Error {
  constructor(
    message = `Dev Proxy Exception, please check if XNAT_DOMAIN is set properly. If XNAT URL is not http://localhost:8080, please set XNAT_DOMAIN`
  ) {
    super(message);
  }
}

export class BadGatewayException extends Error {
  constructor(message = `Please check if you can access the XNAT node.`) {
    super(message);
  }
}
