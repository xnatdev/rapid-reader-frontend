import * as types from './constants/ActionTypes.js';
import actions from './actions.js';

describe('actions', () => {
  it('exports have not changed', () => {
    const expectedExports = [
      'setUser',
      'clearUser',
      'setWorkList',
      'setWorkItemIdx',
      'setWorkItemId',
      'moveToPrevWorkItem',
      'moveToNextWorkItem',
      'setReportForm',
      'removeReportForm',
      'removeAllReportForms',
      'setWorkItem',
      'setWorkItemStatus',
    ].sort();

    const exports = Object.keys(actions).sort();

    expect(exports).toEqual(expectedExports);
  });

  it('should create an action for each', () => {
    const items = [
      {
        action: actions.setUser,
        type: types.SET_USER,
        addl: {
          user: { dummy: true },
        },
      },
      {
        action: actions.clearUser,
        type: types.CLEAR_USER,
        addl: undefined,
      },
      {
        action: actions.setWorkList,
        type: types.SET_WORK_GROUP,
        addl: {
          workList: { dummy: true },
        },
      },
      {
        action: actions.setWorkItemIdx,
        type: types.SET_WORK_ITEM_IDX,
        addl: {
          currentWorkItemIdx: 99,
        },
      },
      {
        action: actions.setWorkItemId,
        type: types.SET_WORK_ITEM_ID,
        addl: {
          currentWorkItemId: 99,
        },
      },
      {
        action: actions.moveToPrevWorkItem,
        type: types.MOVE_TO_PREV_WORK_ITEM,
        addl: undefined,
      },
      {
        action: actions.moveToNextWorkItem,
        type: types.MOVE_TO_NEXT_WORK_ITEM,
        addl: undefined,
      },
      {
        action: actions.setReportForm,
        type: types.SET_REPORT_FORM,
        addl: {
          reportId: 99,
          formData: { dummy: true },
        },
      },
      {
        action: actions.removeReportForm,
        type: types.REMOVE_REPORT_FORM,
        addl: undefined,
      },
      {
        action: actions.removeAllReportForms,
        type: types.REMOVE_ALL_REPORT_FORMS,
        addl: undefined,
      },
      {
        action: actions.setWorkItem,
        type: types.SET_WORK_ITEM,
        addl: {
          workItem: { dummy: true },
        },
      },
      {
        action: actions.setWorkItemStatus,
        type: types.SET_WORK_ITEM_STATUS,
        addl: {
          status: 'dummy',
        },
      },
    ];

    items.forEach(item => {
      const expectedAction = {
        type: item.type,
      };
      if (item.addl) {
        Object.keys(item.addl).forEach(key => {
          expectedAction[key] = item.addl[key];
        });
      }
      let params = [];
      if (item.addl) {
        params = Object.values(item.addl);
      }

      expect(item.action.apply(null, params)).toEqual(expectedAction);
    });
  });
});
