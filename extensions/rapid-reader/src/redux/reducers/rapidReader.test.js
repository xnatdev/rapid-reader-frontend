import { Reducer } from 'redux-testkit';
import cloneDeep from 'lodash.clonedeep';

import reducer, { defaultState } from './rapidReader';
import User from '../../dao/user';
import actions from '../actions.js';

function createDummyWorkList() {
  return {
    items: [],
  };
}

describe('rapidReducer reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  it('should set worklist', () => {
    const initialState = defaultState;

    const workList = createDummyWorkList();
    const action = actions.setWorkList(workList);

    const expectedState = cloneDeep(initialState);
    expectedState.workList = workList;

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expectedState);
  });
});
