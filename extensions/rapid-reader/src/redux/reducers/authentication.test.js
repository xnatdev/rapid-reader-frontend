import { Reducer } from 'redux-testkit';

import reducer, { defaultState } from './authentication';
import User from '../../dao/user';
import actions from '../actions.js';

function createDummyUser() {
  return new User(
    'xnatUrl',
    'username',
    'email',
    'firstName',
    'lastName',
    'token',
    'secret',
    Date.now() + 1000,
    Date.now()
  );
}

describe('authentication reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(defaultState);
  });

  it('should set new data on first SET_USER', () => {
    const initialState = defaultState;

    const user = createDummyUser();
    const action = actions.setUser(user);

    const expectedState = {
      user,
      isLoggedIn: true,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expectedState);
  });

  it('should clear user on CLEAR_USER', () => {
    const user = createDummyUser();

    const initialState = {
      user,
    };

    const action = actions.clearUser();

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(defaultState);
  });

  it('should handle invalid action', () => {
    const invalidAction = { type: 'INVALID_ACTION' };

    Reducer(reducer)
      .withState(defaultState)
      .expect(invalidAction)
      .toReturnState(defaultState);
  });
});
