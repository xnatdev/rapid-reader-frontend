import cloneDeep from 'lodash.clonedeep';
import {
  SET_WORK_GROUP,
  SET_WORK_ITEM_IDX,
  SET_WORK_ITEM_ID,
  MOVE_TO_PREV_WORK_ITEM,
  MOVE_TO_NEXT_WORK_ITEM,
  SET_REPORT_FORM,
  REMOVE_REPORT_FORM,
  REMOVE_ALL_REPORT_FORMS,
  SET_WORK_ITEM,
  SET_WORK_ITEM_STATUS,
} from '../constants/ActionTypes';

export const defaultState = {
  workList: {},
  currentWorkItemIdx: -1,
  currentWorkItemId: -1,
  reports: {},
  workItemStatus: '',
  filteredItems: [],
};

function isFirstWorkItem(state) {
  return state.currentWorkItemIdx <= 0;
}

function isLastWorkItem(state) {
  return (
    state.filteredItems &&
    state.currentWorkItemIdx >= state.filteredItems.length - 1
  );
}

const rapidReader = (state = defaultState, action) => {
  switch (action.type) {
    case SET_WORK_GROUP:
      if (!action.workList) {
        return state;
      } else {
        const workList = cloneDeep(action.workList);
        return {
          ...state,
          workList,
          currentWorkItemIdx: workList.items.length > 0 ? 0 : -1,
          filteredItems: workList.items,
        };
      }

    case SET_WORK_ITEM_IDX:
      if (
        !Number.isInteger(action.currentWorkItemIdx) ||
        action.currentWorkItemIdx < 0 ||
        action.currentWorkItemIdx >= state.filteredItems.length
      ) {
        return state;
      } else {
        return {
          ...state,
          currentWorkItemIdx: action.currentWorkItemIdx,
          currentWorkItemId: state.filteredItems[action.currentWorkItemIdx].id,
        };
      }

    case SET_WORK_ITEM_ID:
      if (
        !Number.isInteger(action.currentWorkItemId) ||
        action.currentWorkItemId < 0
      ) {
        return state;
      } else {
        const {
          workList: { items },
          workItemStatus,
        } = state;
        const newCurrentWorkItemIdx = items.findIndex(
          item => item.id === action.currentWorkItemId
        );
        if (newCurrentWorkItemIdx < 0) {
          return state;
        }
        const newWorkItemStatus =
          workItemStatus &&
          state.workItemStatus !== items[newCurrentWorkItemIdx].status
            ? ''
            : workItemStatus;

        return {
          ...state,
          currentWorkItemId: action.currentWorkItemId,
          currentWorkItemIdx: newCurrentWorkItemIdx,
          workItemStatus: newWorkItemStatus,
        };
      }

    case MOVE_TO_PREV_WORK_ITEM:
      if (isFirstWorkItem(state)) {
        return state;
      } else {
        const currentWorkItemIdx = state.currentWorkItemIdx - 1;
        const currentWorkItemId = state.filteredItems[currentWorkItemIdx].id;
        return {
          ...state,
          currentWorkItemIdx,
          currentWorkItemId,
        };
      }

    case MOVE_TO_NEXT_WORK_ITEM:
      if (isLastWorkItem(state)) {
        return state;
      } else {
        const currentWorkItemIdx = state.currentWorkItemIdx + 1;
        const currentWorkItemId = state.filteredItems[currentWorkItemIdx].id;
        return {
          ...state,
          currentWorkItemIdx,
          currentWorkItemId,
        };
      }

    case SET_REPORT_FORM:
      if (!action.reportId || !action.formData) {
        return state;
      } else {
        const formData = cloneDeep(action.formData);
        const reports = {
          ...cloneDeep(state.reports),
          [action.reportId]: formData,
        };

        return {
          ...state,
          reports,
        };
      }

    case REMOVE_REPORT_FORM:
      if (!action.reportId) {
        return state;
      } else {
        const reports = cloneDeep(state.reports);
        delete reports[action.reportId];

        return {
          ...state,
          reports,
        };
      }

    case REMOVE_ALL_REPORT_FORMS: {
      return {
        ...state,
        reports: {},
      };
    }

    case SET_WORK_ITEM: {
      if (!action.workItem) {
        return state;
      } else {
        const workList = cloneDeep(state.workList);
        const items = workList.items;
        const idx = items.findIndex(item => item.id === action.workItem.id);
        if (idx >= 0) {
          items[idx] = cloneDeep(action.workItem);
        }
        const filteredItems = state.workItemStatus
          ? items.filter(item => item.status === state.workItemStatus)
          : items;

        let currentWorkItemIdx = state.currentWorkItemIdx;
        let currentWorkItemId = -1;
        if (filteredItems.length > currentWorkItemIdx) {
          currentWorkItemId = filteredItems[currentWorkItemIdx].id;
        } else {
          if (filteredItems.length > 0) {
            currentWorkItemId = filteredItems[filteredItems.length - 1].id;
            currentWorkItemIdx = filteredItems.length - 1;
          }
        }
        return {
          ...state,
          workList,
          filteredItems: cloneDeep(filteredItems),
          currentWorkItemIdx,
          currentWorkItemId,
        };
      }
    }

    case SET_WORK_ITEM_STATUS: {
      // status can be '' (All)
      if (action.status === undefined) {
        return state;
      } else {
        const {
          workList: { items = [] },
        } = state;
        const filteredItems = action.status
          ? items.filter(item => item.status === action.status)
          : items;
        const currentWorkItemIdx = filteredItems.length > 0 ? 0 : -1;
        const currentWorkItemId =
          currentWorkItemIdx >= 0 ? filteredItems[currentWorkItemIdx].id : -1;

        return {
          ...state,
          workItemStatus: action.status,
          filteredItems: cloneDeep(filteredItems),
          currentWorkItemIdx,
          currentWorkItemId,
        };
      }
    }

    default:
      return state;
  }
};

export default rapidReader;
