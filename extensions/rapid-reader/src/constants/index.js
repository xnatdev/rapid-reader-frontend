export const WorkListStatus = {
  Open: 'Open',
  //   Partial: 'Partial',
  Reviewing: 'Reviewing',
  Complete: 'Complete',
  Cancelled: 'Cancelled',
};

export const WorkItemStatus = {
  InProgress: 'InProgress',
  Open: 'Open',
  Partial: 'Partial',
  Complete: 'Complete',
  Cancelled: 'Cancelled',
  Failed: 'Failed',
};

export const CommentStatus = {
  Ready: 'Ready',
  Sending: 'Sending',
  Sent: 'Sent',
  Cancelled: 'Cancelled',
};

export const ErrorMessages = {
  RAPID_VIEWER_PLUGIN_NOT_INSTALLED:
    'Rapid Viewer plugin is not installed on XNAT',
  INVALID_XNAT_URL:
    'XNAT URL might not be valid. If the problem persists, please contact Admin.',
};

export const SpecialGroupEmails = {
  TECH: '[TECH]',
};

export const SurveyTypes = {
  PreSurvey: 'PreSurvey',
  PostSurvey: 'PostSurvey',
};
