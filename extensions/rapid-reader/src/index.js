import commandsModule from './commandsModule.js';
import toolbarModule from './toolbarModule.js';
import panelModule from './panelModule.js';

import actions from './redux/actions';

// export {
//   CurrentWorkItemToolbarComponent,
//   ConnectedRapidWorkItemsPanel,
//   RapidRadReportPanel,
//   RapidWorkItemsPanel,
//   RapidWorkItem,
//   XnatLogin,
// } from './components';

import {
  CurrentWorkItemToolbarComponent,
  SurveyComponent,
  ReportComponent,
} from './components';

export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'rapid-reader',

  /**
   *
   *
   * @param {object} [configuration={}]
   * @param {object|array} [configuration.csToolsConfig] - Passed directly to `initCornerstoneTools`
   */
  // preRegistration({ servicesManager, commandsManager, configuration = {} }) {
  //   init({ servicesManager, commandsManager, configuration });
  // },
  getToolbarModule({ servicesManager }) {
    return toolbarModule;
  },
  getCommandsModule({ servicesManager }) {
    return commandsModule;
  },
  getPanelModule({ servicesManager, commandsManager }) {
    return panelModule(servicesManager, commandsManager);
  },
};

export { RAPIDICONS } from './elements';

export {
  PageNotFoundException,
  HttpException,
  UserExpiredException,
} from './exception';

export {
  fetchLogin,
  fetchCSRFToken,
  fetchMetadata,
  fetchPluginCheck,
  fetchRadReportForm,
  fetchTokenInvalidate,
  fetchTokenIssue,
  fetchWorkItems,
  fetchWorkList,
  fetchWorkLists,
  fetchUserInfoForEmbed,
  fetchSurveyTemplateQuestions,
  fetchSurvey,
  fetchPostSurvey,
  fetchUploadPdf,
  fetchBeginTimeTrack,
  fetchUpdateTimeTrack,
} from './fetch';

export { actions };

export { CurrentWorkItemToolbarComponent, SurveyComponent, ReportComponent };

export {
  getRecentLoginInfo,
  saveLoginInfo,
  isWorkItemFinished,
  isTimeOpenEnded,
  calculatedElapsedInMs,
  createPdf,
} from './utils';

export {
  WorkListStatus,
  WorkItemStatus,
  CommentStatus,
  ErrorMessages,
  SurveyTypes,
} from './constants';
