import { connect } from 'react-redux';
import getXnatUrl from '../utils/getXnatUrl';
import XNATStandaloneRouting from '../routes/XNATStandaloneRouting';

const mapStateToProps = state => {
  const xnatUrl = getXnatUrl();

  return {
    xnatUrl,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    activateServer: server => {
      const action = {
        type: 'ACTIVATE_SERVER',
        server,
      };
      dispatch(action);
    },
  };
};

const ConnectedXNATStandaloneRouting = connect(
  mapStateToProps,
  mapDispatchToProps
)(XNATStandaloneRouting);

export default ConnectedXNATStandaloneRouting;
